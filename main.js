// TODO: Select your list element
const results = document.querySelector('#results');



const insertMovies = data => {
  // TODO: write insertMovies function
    // array in Search (one step more)
   data.Search.forEach(result =>{
       
 // Template of your card being injected into the DOM
    const movie = `
      <div class="card-movie">
        <div class="image-top">
          <img height="100%" src="${result.Poster}" />
        </div>
        <div class="content">
          <p>${result.Year}</p>
          <h5>${result.Title}</h5>
        </div>
      </div>
    `;    

//       const movieString=`
//        <div>
//        <img height="200px" src="${movie.Poster}" alt="">
//            <h3>${movie.Title}</h3>
//            <p>${movie.Year}</p>
//        </div>`
       
results.insertAdjacentHTML('beforeend', movie);
       
   })
    
};


const API_key = "51858308";
// This is the API url:
//const URL = `https://www.omdbapi.com/?s=${query}&apikey=${API_key}`;

const fetchMovies = query => {
 // TODO: Write fetch function 
// .then is method that allows you call another function on the value returned from fetching
 fetch(`https://www.omdbapi.com/?s=${query}&apikey=${API_key}`)
     .then(response => response.json())
     .then(insertMovies);
};

//fetchMovies('harry potter'); // on 1st page load

// TODO: Select the form element
const form = document.querySelector('#search-form');
form.addEventListener('submit', (event) => {
  // Stop the submit event from reloading the page
  event.preventDefault();
  // This clears the HTML in the list before new cards are rendered(if not, no rendering)
  results.innerHTML = '';
  // Grab the input
  const input = document.querySelector('#search-input');
  // Call the API, pass it the input
  fetchMovies(input.value);
});

